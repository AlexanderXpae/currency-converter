import UIKit

/// Preparing elements for interaction
class TableViewCell: UITableViewCell {
    
    @IBOutlet weak var codeLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var rateLabel: UILabel!
    @IBOutlet weak var differenceLabel: UILabel!
    @IBOutlet weak var differenceImage: UIImageView!
    
    var differenceModule = DifferenceModule()
    
    func setupData(_ data: Valute) {
        let result = String(format: "%.4f", differenceModule.analyzingTheDifference(data.value, data.previous))
        
        self.codeLabel.text = data.charCode
        self.nameLabel.text = data.name
        self.rateLabel.text = String(data.value)
        
        if Double(result)! >= 0 {
            differenceLabel.textColor = .systemGreen
            differenceImage.tintColor = .systemGreen
            differenceLabel.text = "+\(result)"
        } else {
            differenceLabel.textColor = .systemRed
            differenceImage.tintColor = .systemRed
            differenceLabel.text = "\(result)"
        }
    }
}
