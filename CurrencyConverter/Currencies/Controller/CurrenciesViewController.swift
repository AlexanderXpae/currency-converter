import UIKit

class CurrenciesViewController: UIViewController {

    @IBOutlet weak var outTableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    let reuseIdentifier = "Cell"
    var currencies = [Valute]()
    var currenciesOut = [Valute]()
    var filteredCurrencies = [Valute]()
    
    let cvc = ConverterViewController()
    let dc = DataSearcher()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupResources()
        setupNetworkSrvice()
    }
    
    /// Connection delegate, dataSource and  cells
    func setupResources() {
        searchBar.delegate = self
        outTableView.dataSource = self
        
        let nib = UINib(nibName: "TableViewCell", bundle: nil)
        outTableView?.register(nib, forCellReuseIdentifier: reuseIdentifier)
    }
    
    /// Request for data retrieval
    func setupNetworkSrvice() {
        filteredCurrencies.removeAll()
        CurrenciesNetworkService.getCurrencies { (response) in
            self.currencies = response.currencies
            self.reloadData()
        }
    }
    
    func reloadData() {
        DispatchQueue.main.async {
            self.outTableView.reloadData()
        }
    }
    
    /// Request to receive data by clicking on a button and receive a notification
    @IBAction func reloadData(_ sender: Any) {
        setupNetworkSrvice()
        
        let alert = UIAlertController.init(title: "Information", message: "Information has been successfully updated!", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}

extension CurrenciesViewController: UITableViewDataSource {
    func checkData() {
        if !filteredCurrencies.isEmpty {
            currenciesOut = filteredCurrencies
        } else {
            currenciesOut = currencies
        }
    }
    /// Setting the number of filled and processed cells
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        checkData()
        return self.currenciesOut.count
    }
    
    /// Filling cells with information
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.outTableView.dequeueReusableCell(withIdentifier: self.reuseIdentifier, for: indexPath) as? TableViewCell
        cell?.setupData(self.currenciesOut[indexPath.row])
        return cell ?? UITableViewCell()
    }
}

extension CurrenciesViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.count != 0 {
            let array = dc.getFilteredCurrencies(currencies, searchText)
            filteredCurrencies = array
            self.reloadData()
        } else {
            setupNetworkSrvice()
        }
    }
}
