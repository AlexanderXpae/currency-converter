//
//  NetworkService.swift
//  CurrencyConverter
//
//  Created by Alexander Xpae on 06.04.2021.
//

import Foundation

class CurrenciesNetworkService {

    private init() {}
    
    static func getCurrencies(comletion: @escaping(GetCurrenciesResponse) -> ()) {
        guard let url = URL(string: "https://bitpay.com/api/rates") else { return }
        
        NetworkService.shared.getData(url) { (json) in
            do {
                let response = try GetCurrenciesResponse(json)
                comletion(response)
            } catch {
                print(error)
            }
        }
    }
}
