import UIKit

class ConverterViewController: UIViewController {
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var outCollectionView: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var enterTextField: UITextField!
    @IBOutlet weak var getResultButtonStyle: UIButton!
    @IBOutlet weak var resultLabel: UILabel!
    @IBOutlet weak var mainCurrenciesNameLabel: UILabel!
    @IBOutlet weak var secondCurrenciesNameLabel: UILabel!
    
    let reuseIdentifier = "Cell"
    var currencies = [Valute]()
    var currenciesOut = [Valute]()
    var filteredCurrencies = [Valute]()
    var selectedCurrencies = [Valute]()
    
    let cvc = CollectionViewCell()
    let dc = DataSearcher()
    var logicModule = LogicModule()
    
    var reversInfo = false
    let clearValue = "1.0"
    
    override func viewWillAppear(_ animated: Bool) {
        setupNetworkSrvice()
        setupResources()
        setupData()
    }
    
    /// Request for data retrieval
    func setupNetworkSrvice() {
        filteredCurrencies.removeAll()
        CurrenciesNetworkService.getCurrencies { (response) in
            self.currencies = response.currencies
            self.reloadData()
        }
    }
    
    /// Setup delegate
    func setupResources() {
        searchBar.delegate = self
        enterTextField.delegate = self
        outCollectionView.dataSource = self
        outCollectionView.delegate = self
        
        let nib = UINib(nibName: "CollectionViewCell", bundle: nil)
        outCollectionView?.register(nib, forCellWithReuseIdentifier: reuseIdentifier)
    }
    
    /// Setting initial values
    func setupData() {
        mainCurrenciesNameLabel.text = "RUS"
        enterTextField.text = "1.0"
    }
    
    @IBAction func getResultButton(_ sender: UIButton) {
        getResult()
    }
    
    func getResult() {
        resultLabel.text = String(logicModule.getResult(Double(enterTextField.text!)!, reversInfo, selectedCurrencies[0].value, selectedCurrencies[0].nominal))
        getResultButtonStyle.tintColor = .systemGreen
    }
    
    @IBAction func reversButton(_ sender: UIButton) {
        reversData()
    }
    
    func reversData() {
        reversInfo = !reversInfo
        
        let temporaryName: String
        
        temporaryName = mainCurrenciesNameLabel.text!
        mainCurrenciesNameLabel.text = secondCurrenciesNameLabel.text
        secondCurrenciesNameLabel.text = temporaryName
        
        getResultButtonStyle.tintColor = .systemGreen
        clearObjects()
        getResult()
    }
    
    func clearObjects() {
        enterTextField.text = clearValue
        resultLabel.text = clearValue
    }
    
    func reloadData() {
        DispatchQueue.main.async {
            self.outCollectionView.reloadData()
        }
    }
    
    /// Request to receive data by clicking on a button and receive a notification
    @IBAction func reloadData(_ sender: Any) {
        setupNetworkSrvice()
        
        let alert = UIAlertController.init(title: "Information", message: "Information has been successfully updated!", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}

extension ConverterViewController: UICollectionViewDataSource {
    func checkData() {
        if !filteredCurrencies.isEmpty {
            currenciesOut = filteredCurrencies
        } else {
            currenciesOut = currencies
        }
        
        pageControl.numberOfPages = currenciesOut.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        checkData()
        return self.currenciesOut.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.outCollectionView.dequeueReusableCell(withReuseIdentifier: self.reuseIdentifier, for: indexPath) as? CollectionViewCell
        cell?.setupData(self.currenciesOut[indexPath.row])
        return cell ?? UICollectionViewCell()
    }
}


extension ConverterViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        pageControl.currentPage = indexPath.row
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if !selectedCurrencies.isEmpty {
            selectedCurrencies.removeAll()
        }
        
        selectedCurrencies.append(currenciesOut[indexPath.row])
        reversInfo = false
    
        secondCurrenciesNameLabel.text = selectedCurrencies[0].charCode
        setupData()
        getResult()
    }
}

extension ConverterViewController: UITextFieldDelegate {
    /// Control of entered characters
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        func finalPosition() {
            enterTextField.endEditing(true)
        }
        
        if enterTextField.text!.count == 10 && reversInfo == false {
            finalPosition()
        }
        
        getResultButtonStyle.tintColor = .lightGray
        
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        getResultButtonStyle.tintColor = .lightGray
        
        clearObjects()
    }
}

extension ConverterViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.count != 0 {
            let array = dc.getFilteredCurrencies(currencies, searchText)
            filteredCurrencies = array
            self.reloadData()
        } else {
            setupNetworkSrvice()
        }
    }
}

