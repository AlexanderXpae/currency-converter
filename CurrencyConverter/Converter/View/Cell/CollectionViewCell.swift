//
//  CollectionViewCell.swift
//  CurrencyConverter
//
//  Created by Alexander Xpae on 12.04.2021.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var codeLabel: UILabel!
    @IBOutlet weak var rateLabel: UILabel!
    @IBOutlet weak var differenceImage: UIImageView!
    
    var differenceModule = DifferenceModule()

    func setupData(_ data: Valute) {
        let result = String(format: "%.4f", differenceModule.analyzingTheDifference(data.value, data.previous))
        
        self.codeLabel.text = data.charCode
        self.rateLabel.text = String(data.value)
        
        if Double(result)! >= 0 {
            differenceImage.tintColor = .systemGreen
        } else {
            differenceImage.tintColor = .systemRed
        }
    }
}
