import Foundation

class LogicModule: NSObject {
    
    /// Calculation of the result from the input data
    public func getResult(_ enterValue: Double, _ reversInfo: Bool, _ value: Double, _ nominal: Int) -> String {
        let result: Double
        var resultOut: String
        
        if reversInfo == false {
            result = (1 / (value / Double(nominal))) * enterValue
        } else {
            
            result = (value / Double(nominal)) * enterValue
        }
        
        resultOut = String(format: "%.4f", result)

        return resultOut
    }
}
