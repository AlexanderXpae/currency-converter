import Foundation

import UIKit

class ConverterDataFetcher: NSObject {
    private var currenciesData: CurrenciesDataFetcher?
    
    /// Getting a position for processing further actions
    func setRepistory(_ data:  CurrenciesDataFetcher?) {
        currenciesData = data
    }
    
    /// Retrieving information about a specific cell of a dictionary
    func getRepistory() -> CurrenciesDataFetcher? {
        return currenciesData
    }
}
