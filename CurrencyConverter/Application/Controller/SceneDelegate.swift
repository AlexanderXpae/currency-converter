import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?

    
    /// Setting up the home screen
    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        guard let windowScene = (scene as? UIWindowScene) else { return }
        window = UIWindow(frame: windowScene.coordinateSpace.bounds)
        window?.windowScene = windowScene
        
        let appTabBar = UITabBarController()
        
        let currenciesViewController = CurrenciesViewController()
        let currenciesTabBarItem = UITabBarItem(title: "Currencies", image: UIImage(systemName: "list.bullet"), selectedImage: nil)
        currenciesViewController.tabBarItem = currenciesTabBarItem
        
        let converterViewController = ConverterViewController()
        let converterTabBarItem = UITabBarItem(title: "Converter", image: UIImage(systemName: "slider.horizontal.3"), selectedImage: nil)
        converterViewController.tabBarItem = converterTabBarItem
        
        appTabBar.viewControllers = [currenciesViewController, converterViewController]
        appTabBar.tabBar.tintColor = .black
        appTabBar.selectedIndex = 0
        
        window?.rootViewController = appTabBar
        window?.makeKeyAndVisible()
    }
}

