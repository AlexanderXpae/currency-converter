import UIKit

/// Unpacking and processing the received response
struct GetCurrenciesResponse {
        
    var currencies = [Valute]()

    init(_ json: Any) throws {
        guard let array = json as? [String: Valute] else { throw AplicationError.jsonError }
        
        let keysSorted = array.keys.sorted { $0.localizedCaseInsensitiveCompare($1) == ComparisonResult.orderedAscending }
        var arraySorted = [Valute]()
        
        for name in keysSorted {
            guard !name.isEmpty else { continue }
            arraySorted.append(array[name]!)
        }
        
        currencies = arraySorted
    }
}

