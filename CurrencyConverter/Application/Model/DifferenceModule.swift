import Foundation

class DifferenceModule: NSObject {
    func analyzingTheDifference(_ value: Double, _ previous: Double) -> Double {
        return value - previous
    }
}
