import Foundation

class CurrenciesNetworkService {
    
    private init() {}
    
    /// Transmitting the URL address to receive a response
    static func getCurrencies(comletion: @escaping(GetCurrenciesResponse) -> ()) {
        guard let url = URL(string: "https://www.cbr-xml-daily.ru/daily_json.js") else { return }
        
        NetworkSrvice.shared.getData(url) { (json) in
            do {
                let response = try GetCurrenciesResponse(json)
                comletion(response)
            } catch {
                print(error)
            }
        }
    }
}
