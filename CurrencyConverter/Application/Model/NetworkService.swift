import Foundation

class NetworkSrvice {
    
    private init() {}
    static let shared = NetworkSrvice()
    
    /// Connecting to the API and receiving a response
    /// - Returns: JSON
    public func getData(_ url: URL, copletion: @escaping (Any) -> ()) {
        let session = URLSession.shared
        
        session.dataTask(with: url) { (data, response, error) in
            guard let data = data else { return }
            
            do {
                let json = try JSONDecoder().decode(DigestAuthSuccess.self, from: data)
                
                DispatchQueue.main.async {
                    copletion(json.valute)
                }
            } catch {
                print(error)
            }
        }.resume()
    }
}
