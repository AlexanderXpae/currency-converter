import Foundation

class DataSearcher: NSObject {
    var filteredCurrencies = [Valute]()
    
    func getFilteredCurrencies(_ array: [Valute], _ searchText: String) -> [Valute] {
        if !filteredCurrencies.isEmpty {
            filteredCurrencies.removeAll()
        }
        
        for id in 0...array.count - 1 {
            if array[id].charCode.lowercased().contains(searchText.lowercased()) {
                filteredCurrencies.append(array[id])
            }
        }
        
        return filteredCurrencies
    }
}
