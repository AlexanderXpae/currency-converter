import Foundation

// MARK: - DigestAuthSuccess
struct DigestAuthSuccess: Codable {
    let valute: [String: Valute]

    enum CodingKeys: String, CodingKey {
        case valute = "Valute"
    }
}

// MARK: - Valute
struct Valute: Codable {
    let charCode: String
    let nominal: Int
    let name: String
    let value, previous: Double

    enum CodingKeys: String, CodingKey {
        case charCode = "CharCode"
        case nominal = "Nominal"
        case name = "Name"
        case value = "Value"
        case previous = "Previous"
    }
}

// MARK: - BasicAuth
struct BasicAuth: Codable {
    let authenticated: Bool
}

// MARK: - OAuth10VerifySignature
struct OAuth10VerifySignature: Codable {
    let status, message: String
    let baseURI: String?
    let normalizedParamString, baseString, signingKey: String?

    enum CodingKeys: String, CodingKey {
        case status, message
        case baseURI = "base_uri"
        case normalizedParamString = "normalized_param_string"
        case baseString = "base_string"
        case signingKey = "signing_key"
    }
}

// MARK: - HawkAuth
struct HawkAuth: Codable {
    let status, message: String
}

// MARK: - SetCookies
struct SetCookies: Codable {
    let cookies: SetCookiesCookies
}

// MARK: - SetCookiesCookies
struct SetCookiesCookies: Codable {
    let foo1, foo2: String
}

// MARK: - GetCookies
struct GetCookies: Codable {
    let cookies: GetCookiesCookies
}

// MARK: - GetCookiesCookies
struct GetCookiesCookies: Codable {
    let foo2: String
}

// MARK: - DeleteCookies
struct DeleteCookies: Codable {
    let cookies: GetCookiesCookies
}

// MARK: - RequestHeaders
struct RequestHeaders: Codable {
    let headers: Headers
}

// MARK: - Headers
struct Headers: Codable {
    let host, accept, acceptEncoding, acceptLanguage: String
    let cacheControl, mySampleHeader, postmanToken, userAgent: String
    let xForwardedPort, xForwardedProto: String

    enum CodingKeys: String, CodingKey {
        case host, accept
        case acceptEncoding = "accept-encoding"
        case acceptLanguage = "accept-language"
        case cacheControl = "cache-control"
        case mySampleHeader = "my-sample-header"
        case postmanToken = "postman-token"
        case userAgent = "user-agent"
        case xForwardedPort = "x-forwarded-port"
        case xForwardedProto = "x-forwarded-proto"
    }
}

// MARK: - ResponseHeaders
struct ResponseHeaders: Codable {
    let contentType, test: String

    enum CodingKeys: String, CodingKey {
        case contentType = "Content-Type"
        case test
    }
}

// MARK: - ResponseStatusCode
struct ResponseStatusCode: Codable {
    let status: Int
}

// MARK: - DelayResponse
struct DelayResponse: Codable {
    let delay: String
}

// MARK: - TimestampValidity
struct TimestampValidity: Codable {
    let valid: Bool
}

// MARK: - TransformCollectionFromFormatV1ToV2
struct TransformCollectionFromFormatV1ToV2: Codable {
    let variables: [JSONAny]
    let info: Info
    let item: [Item]
}

// MARK: - Info
struct Info: Codable {
    let name, postmanID, infoDescription: String
    let schema: String

    enum CodingKeys: String, CodingKey {
        case name
        case postmanID = "_postman_id"
        case infoDescription = "description"
        case schema
    }
}

// MARK: - Item
struct Item: Codable {
    let name: String
    let event: [Event]?
    let request: ItemRequest
    let response: [JSONAny]
}

// MARK: - Event
struct Event: Codable {
    let listen: String
    let script: Script
}

// MARK: - Script
struct Script: Codable {
    let type: String
    let exec: [String]
}

// MARK: - ItemRequest
struct ItemRequest: Codable {
    let url: String
    let method: String
    let header: [Header]
    let body: Body
}

// MARK: - Body
struct Body: Codable {
    let mode, raw: String
}

// MARK: - Header
struct Header: Codable {
    let key, value, headerDescription: String

    enum CodingKeys: String, CodingKey {
        case key, value
        case headerDescription = "description"
    }
}

// MARK: - TransformCollectionFromFormatV2ToV1
struct TransformCollectionFromFormatV2ToV1: Codable {
    let id, name, transformCollectionFromFormatV2ToV1Description: String
    let order: [String]
    let folders: [JSONAny]
    let requests: [RequestElement]

    enum CodingKeys: String, CodingKey {
        case id, name
        case transformCollectionFromFormatV2ToV1Description = "description"
        case order, folders, requests
    }
}

// MARK: - RequestElement
struct RequestElement: Codable {
    let id, name, collectionID, method: String
    let headers: String
    let data: [JSONAny]
    let rawModeData: String
    let tests, preRequestScript: String?
    let url: String
    let dataMode: String?

    enum CodingKeys: String, CodingKey {
        case id, name
        case collectionID = "collectionId"
        case method, headers, data, rawModeData, tests, preRequestScript, url, dataMode
    }
}

// MARK: - Encode/decode helpers
class JSONCodingKey: CodingKey {
    let key: String

    required init?(intValue: Int) {
        return nil
    }

    required init?(stringValue: String) {
        key = stringValue
    }

    var intValue: Int? {
        return nil
    }

    var stringValue: String {
        return key
    }
}

class JSONAny: Codable {

    let value: Any

    static func decodingError(forCodingPath codingPath: [CodingKey]) -> DecodingError {
        let context = DecodingError.Context(codingPath: codingPath, debugDescription: "Cannot decode JSONAny")
        return DecodingError.typeMismatch(JSONAny.self, context)
    }

    static func encodingError(forValue value: Any, codingPath: [CodingKey]) -> EncodingError {
        let context = EncodingError.Context(codingPath: codingPath, debugDescription: "Cannot encode JSONAny")
        return EncodingError.invalidValue(value, context)
    }

    static func decode(from container: SingleValueDecodingContainer) throws -> Any {
        if let value = try? container.decode(Bool.self) {
            return value
        }
        if let value = try? container.decode(Int64.self) {
            return value
        }
        if let value = try? container.decode(Double.self) {
            return value
        }
        if let value = try? container.decode(String.self) {
            return value
        }
        throw decodingError(forCodingPath: container.codingPath)
    }

    static func decode(from container: inout UnkeyedDecodingContainer) throws -> Any {
        if let value = try? container.decode(Bool.self) {
            return value
        }
        if let value = try? container.decode(Int64.self) {
            return value
        }
        if let value = try? container.decode(Double.self) {
            return value
        }
        if let value = try? container.decode(String.self) {
            return value
        }
        if var container = try? container.nestedUnkeyedContainer() {
            return try decodeArray(from: &container)
        }
        if var container = try? container.nestedContainer(keyedBy: JSONCodingKey.self) {
            return try decodeDictionary(from: &container)
        }
        throw decodingError(forCodingPath: container.codingPath)
    }

    static func decode(from container: inout KeyedDecodingContainer<JSONCodingKey>, forKey key: JSONCodingKey) throws -> Any {
        if let value = try? container.decode(Bool.self, forKey: key) {
            return value
        }
        if let value = try? container.decode(Int64.self, forKey: key) {
            return value
        }
        if let value = try? container.decode(Double.self, forKey: key) {
            return value
        }
        if let value = try? container.decode(String.self, forKey: key) {
            return value
        }
        if var container = try? container.nestedUnkeyedContainer(forKey: key) {
            return try decodeArray(from: &container)
        }
        if var container = try? container.nestedContainer(keyedBy: JSONCodingKey.self, forKey: key) {
            return try decodeDictionary(from: &container)
        }
        throw decodingError(forCodingPath: container.codingPath)
    }

    static func decodeArray(from container: inout UnkeyedDecodingContainer) throws -> [Any] {
        var arr: [Any] = []
        while !container.isAtEnd {
            let value = try decode(from: &container)
            arr.append(value)
        }
        return arr
    }

    static func decodeDictionary(from container: inout KeyedDecodingContainer<JSONCodingKey>) throws -> [String: Any] {
        var dict = [String: Any]()
        for key in container.allKeys {
            let value = try decode(from: &container, forKey: key)
            dict[key.stringValue] = value
        }
        return dict
    }

    static func encode(to container: inout UnkeyedEncodingContainer, array: [Any]) throws {
        for value in array {
            if let value = value as? Bool {
                try container.encode(value)
            } else if let value = value as? Int64 {
                try container.encode(value)
            } else if let value = value as? Double {
                try container.encode(value)
            } else if let value = value as? String {
                try container.encode(value)
            } else if let value = value as? [Any] {
                var container = container.nestedUnkeyedContainer()
                try encode(to: &container, array: value)
            } else if let value = value as? [String: Any] {
                var container = container.nestedContainer(keyedBy: JSONCodingKey.self)
                try encode(to: &container, dictionary: value)
            } else {
                throw encodingError(forValue: value, codingPath: container.codingPath)
            }
        }
    }

    static func encode(to container: inout KeyedEncodingContainer<JSONCodingKey>, dictionary: [String: Any]) throws {
        for (key, value) in dictionary {
            let key = JSONCodingKey(stringValue: key)!
            if let value = value as? Bool {
                try container.encode(value, forKey: key)
            } else if let value = value as? Int64 {
                try container.encode(value, forKey: key)
            } else if let value = value as? Double {
                try container.encode(value, forKey: key)
            } else if let value = value as? String {
                try container.encode(value, forKey: key)
            } else if let value = value as? [Any] {
                var container = container.nestedUnkeyedContainer(forKey: key)
                try encode(to: &container, array: value)
            } else if let value = value as? [String: Any] {
                var container = container.nestedContainer(keyedBy: JSONCodingKey.self, forKey: key)
                try encode(to: &container, dictionary: value)
            } else {
                throw encodingError(forValue: value, codingPath: container.codingPath)
            }
        }
    }

    static func encode(to container: inout SingleValueEncodingContainer, value: Any) throws {
        if let value = value as? Bool {
            try container.encode(value)
        } else if let value = value as? Int64 {
            try container.encode(value)
        } else if let value = value as? Double {
            try container.encode(value)
        } else if let value = value as? String {
            try container.encode(value)
        } else {
            throw encodingError(forValue: value, codingPath: container.codingPath)
        }
    }

    public required init(from decoder: Decoder) throws {
        if var arrayContainer = try? decoder.unkeyedContainer() {
            self.value = try JSONAny.decodeArray(from: &arrayContainer)
        } else if var container = try? decoder.container(keyedBy: JSONCodingKey.self) {
            self.value = try JSONAny.decodeDictionary(from: &container)
        } else {
            let container = try decoder.singleValueContainer()
            self.value = try JSONAny.decode(from: container)
        }
    }

    public func encode(to encoder: Encoder) throws {
        if let arr = self.value as? [Any] {
            var container = encoder.unkeyedContainer()
            try JSONAny.encode(to: &container, array: arr)
        } else if let dict = self.value as? [String: Any] {
            var container = encoder.container(keyedBy: JSONCodingKey.self)
            try JSONAny.encode(to: &container, dictionary: dict)
        } else {
            var container = encoder.singleValueContainer()
            try JSONAny.encode(to: &container, value: self.value)
        }
    }
}
