import Foundation

/// Data dictionary initializer
struct CurrenciesDataFetcher {
    var code: String
    var name: String
    var rate: Double
    
    init?(dictionary: [String: AnyObject]) {
        guard let code = dictionary["code"] as? String,
              let name = dictionary["name"] as? String,
              let rate = dictionary["rate"] as? Double
        else { return nil }
        
        self.code = code
        self.name = name
        self.rate = rate
    }
}
